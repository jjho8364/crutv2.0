package com.crucru.crutv20.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;


import com.crucru.crutv20.R;
import com.crucru.crutv20.activity.VideoViewActivity;
import com.crucru.crutv20.adapter.Fr05ListViewAdapter;
import com.crucru.crutv20.models.Fr05ListViewItem;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-09.
 */
public class Fragment05 extends Fragment implements View.OnClickListener {

    private final String TAG = " Fragment05 - ";
    private ProgressDialog mProgressDialog;
    private ArrayList<Fr05ListViewItem> listArr;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        TelephonyManager telephony = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephony.getNetworkOperator();
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        boolean phoneNumFlag = false;
        boolean operatorFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        String phoneNum = telManager.getLine1Number();
        if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        if(operator == null || operator.equals("")){ operatorFlag = true; }
        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "operatorFlag : " + operatorFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if ((phoneNumFlag || contactsFlag || operatorFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment05, container, false);


            listArr = new ArrayList<Fr05ListViewItem>();
            listArr.add(new Fr05ListViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/sbs-banner.jpg", "http://www.sinktv.com/ch/korea/sbs.php"));
            listArr.add(new Fr05ListViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/kbs1-banner.jpg", "http://www.sinktv.com/ch/korea/kbs1.php"));
            listArr.add(new Fr05ListViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/kbs2-banner.jpg", "http://www.sinktv.com/ch/korea/kbs2.php"));
            listArr.add(new Fr05ListViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/mbc-banner.jpg", "http://www.sinktv.com/ch/korea/mbc.php"));
            listArr.add(new Fr05ListViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/jtbc-banner.jpg", "http://www.sinktv.com/ch/korea/jtbc.php"));
            listArr.add(new Fr05ListViewItem("http://www.sinktv.com/wp-content/uploads/2014/03/channela-banner.jpg", "http://www.sinktv.com/ch/korea/channela.php"));
            listArr.add(new Fr05ListViewItem("http://www.sinktv.com/wp-content/uploads/2016/04/ytn-banner.jpg", "http://www.sinktv.com/ch/korea/ytn.php"));
            ListView listView = (ListView)view.findViewById(R.id.fr05_listview);
            listView.setAdapter(new Fr05ListViewAdapter(getActivity(), getActivity().getLayoutInflater(), listArr));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d(TAG, "videoUrl : " + listArr.get(position).getVideoUrl());
                    Intent intent = new Intent(getActivity(), VideoViewActivity.class);
                    intent.putExtra("videoUrl", listArr.get(position).getVideoUrl());
                    intent.putExtra("flag", true);
                    startActivity(intent);
                }
            });


            // http://www.sinktv.com/ch/korea/jtbc.php

            /*ImageView sbs = (ImageView)view.findViewById(R.id.img_sbs);
            ImageView mbc = (ImageView)view.findViewById(R.id.img_mbc);
            ImageView kbs1 = (ImageView)view.findViewById(R.id.img_kbs1);
            ImageView kbs2 = (ImageView)view.findViewById(R.id.img_kbs2);
            ImageView jtbc = (ImageView)view.findViewById(R.id.img_jtbc);
            ImageView ytn = (ImageView)view.findViewById(R.id.img_ytn);

            sbs.setImageResource(R.drawable.sbs);
            mbc.setImageResource(R.drawable.mbc);
            kbs1.setImageResource(R.drawable.kbs1);
            kbs2.setImageResource(R.drawable.kbs2);
            jtbc.setImageResource(R.drawable.jtbc);
            ytn.setImageResource(R.drawable.ytn);*/

            /*sbs.setOnClickListener(this);
            mbc.setOnClickListener(this);
            kbs1.setOnClickListener(this);
            kbs2.setOnClickListener(this);
            jtbc.setOnClickListener(this);
            ytn.setOnClickListener(this);*/


        }



        return view;

    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){


        }
    }


}
