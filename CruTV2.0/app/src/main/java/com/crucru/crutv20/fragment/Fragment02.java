package com.crucru.crutv20.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.crucru.crutv20.R;
import com.crucru.crutv20.activity.TvListActivity;
import com.crucru.crutv20.adapter.ListViewAdapter;
import com.crucru.crutv20.models.ListViewItem;
import com.crucru.crutv20.models.PagingModel;
import com.crucru.crutv20.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-08.
 */
public class Fragment02 extends Fragment {

    private final String TAG = " Fragment02 - ";
    private ProgressDialog mProgressDialog;
    private ArrayList<ListViewItem> listArr;
    private final String baseUrl = "http://alinktv.org";
    private String baseMidUrl = "/main/category/";
    private String categoryUrl = "2";
    private ListView listView;
    private LinearLayout linearLayout;
    private ArrayList<PagingModel> pagingArr;
    private GetListView gridListView = null;

    private boolean clickPaging = false;
    private String pagingUrl = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        TelephonyManager telephony = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String operator = telephony.getNetworkOperator();
        int portrait_width_pixel= Math.min(this.getResources().getDisplayMetrics().widthPixels, this.getResources().getDisplayMetrics().heightPixels);
        int dots_per_virtual_inch=this.getResources().getDisplayMetrics().densityDpi;
        boolean isPhone = true;
        boolean contactsFlag = false;
        boolean phoneNumFlag = false;
        boolean operatorFlag = false;
        float virutal_width_inch=portrait_width_pixel/dots_per_virtual_inch;
        if (virutal_width_inch <= 2) { isPhone = true; } else { isPhone = false; }
        Cursor c = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        int contactsCnt = c.getCount();
        if(contactsCnt < 20){ contactsFlag = true; }
        TelephonyManager telManager = (TelephonyManager)getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
        String phoneNum = telManager.getLine1Number();
        if(phoneNum == null || phoneNum.equals("")){ phoneNumFlag = true; }
        if(operator == null || operator.equals("")){ operatorFlag = true; }
        Log.d(TAG, "contactsFlag : " + contactsFlag);
        Log.d(TAG, "operatorFlag : " + operatorFlag);
        Log.d(TAG, "isPhone : " + isPhone);
        Log.d(TAG, "phoneNumFlag : " + phoneNumFlag);
        if ((phoneNumFlag || contactsFlag || operatorFlag) && isPhone) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment01, container, false);
            listView = (ListView)view.findViewById(R.id.listview);
            linearLayout = (LinearLayout)view.findViewById(R.id.li_paging);

            gridListView = new GetListView();//.execute();
            gridListView.execute();
        }



        return view;

    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();

            if(((LinearLayout) linearLayout).getChildCount() > 0){
                ((LinearLayout) linearLayout).removeAllViews();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {

            listArr = null;
            listArr = new ArrayList<ListViewItem>();
            pagingArr = null;
            pagingArr = new ArrayList<PagingModel>();


            String url = "";

            if(clickPaging){
                url = baseUrl + pagingUrl;
                clickPaging = false;
            } else {
                url = baseUrl + baseMidUrl + categoryUrl;
            }

            Document doc = null;

            try {
                doc = Jsoup.connect(url).timeout(10000).userAgent("Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0").get();
                Elements main = doc.select(".list-group");
                Elements divs = main.select("a");
                // 페이징
                Elements paging = doc.select(".pagination");
                Elements lis = paging.select("li");

                for(int i=0 ; i<divs.size() ; i++){
                    String title = divs.get(i).text();
                    String linkUrl = divs.get(i).attr("href");

                    listArr.add(new ListViewItem(title, linkUrl));
                    //Log.d(TAG, "title : " + title);
                    //Log.d(TAG, "linkUrl : " + linkUrl);
                }

                for(int i=0 ; i<lis.size() ; i++){
                    String pageNum = lis.get(i).text().split("[(]")[0];
                    String pageLinkUrl = lis.get(i).select("a").attr("href");

                    pagingArr.add(new PagingModel(pageLinkUrl, pageNum));
                    //Log.d(TAG, "pageNum : " + pageNum);
                    //Log.d(TAG, "pageLinkUrl : " + pageLinkUrl);

                }



            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // 페이징 버튼 동적 생성
            for(int i=0 ; i<pagingArr.size() ; i++){
                Button btn = new Button(getActivity());
                btn.setLayoutParams(new LinearLayout.LayoutParams(120, 140));
                btn.setText(pagingArr.get(i).getPageNum());
                btn.setTextSize(10);
                final String tempUrl = pagingArr.get(i).getLinkUrl();
                if(tempUrl.equals("#")){
                    btn.setBackgroundResource(R.color.pink_300);
                }

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Log.d(TAG, "clicked pageBtn : " + tempUrl);
                        clickPaging = true;
                        pagingUrl = tempUrl;
                        if(!pagingUrl.equals("#")){
                            if(pagingUrl.equals("1")){
                                clickPaging = false;
                            }
                            new GetListView().execute();
                        }

                    }
                });

                linearLayout.addView(btn);
            }


            // adapter에 적용
            listView.setAdapter(new ListViewAdapter(getActivity(), getActivity().getLayoutInflater(), listArr));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(NetworkUtil.getConnectivity(getActivity())){
                        String linkUrl = baseUrl + listArr.get(position).getLinkUrl();
                        //Log.d(TAG, "linkUrl : " + linkUrl);

                        Intent intent = new Intent(getActivity(), TvListActivity.class);
                        intent.putExtra("linkUrl", linkUrl);
                        //Log.d(TAG, linkUrl);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "네트워크 상태를 확인 하세요.", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            mProgressDialog.dismiss();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(gridListView != null){
            gridListView.cancel(true);
        }
    }

}
